from os.path import isfile
import requests
import sqlite3
from faker import Faker



def get_requirements() -> str:
    with open('requirements.txt') as file:
        text = file.read()
    return text

def generate_fake_names(num:int) -> list[str]:
    fake = Faker()
    names_list = [fake.name() for _ in range(num)]
    return names_list

def generate_fake_emails(num:int) -> list[str]:
    fake = Faker()
    emails_list = [fake.ascii_email() for _ in range(num)]
    return emails_list

def generate_fake_users(num:int) -> str:
    names = generate_fake_names(num)
    emails = generate_fake_emails(num)
    users = [f'User: {name=}, {email=}' for name, email in zip(names, emails)]
    return '; '.join(users)

def request_people_in_space_number():
    response = requests.get('http://api.open-notify.org/astros.json')
    return str(response.json()["number"])

def sql_request(request):
    con = sqlite3.connect('phones.db')
    cur = con.cursor()
    response = cur.execute(request).fetchall()
    con.commit()
    con.close()
    return response
