from flask import Flask, request
from utils import get_requirements, generate_fake_users, request_people_in_space_number, sql_request

app = Flask(__name__)

@app.route("/requirements")
def requirements():
    return get_requirements()

@app.route("/generate-users")
def generate_users():
    number = request.args.get('number', '100')
    if number[0] == '-' and number[1:].isdigit() or number == '0':
        return 'Number must be positive'
    if not number.isdigit():
        return 'Incorrect value for number'
    if int(number) > 1000:
        return 'Too big number'
    return generate_fake_users(int(number))

@app.route("/space")
def space():
    number = request_people_in_space_number()
    return f'There are {number} people in space now'

@app.route('/phones/create')
def phones_create():
    name, value = request.args.get('name', 'John'), request.args.get('value', '+123456789012')
    req = f'''
    INSERT INTO Phones ('contact name', 'phoneValue')
    VALUES ('{name}', '{value}')
    '''
    sql_request(req)
    return ''

@app.route('/phones/read')
def phones_read():
    req = f'''
    SELECT * FROM Phones
    '''
    response = sql_request(req)
    return str(response)

@app.route('/phones/update')
def phones_update():
    name, value, phone_id = (
        request.args.get('name', None), request.args.get('value', None), request.args.get('id', None)
    )
    if not phone_id:
        return 'Please input phone id'
    req = f'UPDATE Phones\n'
    if name:
        req += f"SET 'contact name' = '{name}'\n"
    if value:
        req += f"SET 'phoneValue' = '{value}'\n"
    req += f'WHERE phoneID = {phone_id};'
    sql_request(req)
    return ''

@app.route('/phones/delete')
def phones_delete():
    phone_id = request.args.get('id', None)
    if not phone_id:
        return 'Please input phone id'
    req = f'DELETE FROM Phones WHERE phoneID = {phone_id};'
    sql_request(req)
    return ''

if __name__ == '__main__':
    app.run()